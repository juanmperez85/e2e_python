from core.base_page import BasePage


class BasePageWikia(BasePage):
    def get_base_url(self, context):
        base_url = context.config.get('YOURPAGE', 'URL_YOURPAGE')
        return base_url