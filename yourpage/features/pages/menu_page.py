from __future__ import unicode_literals
from core.base_page import BasePage


class MenuPage(BasePage):
    menu = "nav[class='wds-global-navigation__content-bar']"

    games_link = "a[href='//fandom.yourpage.com/topics/games']"
    movies_link = "a[href='//fandom.yourpage.com/topics/games']"
    tv_link = "a[href='//fandom.yourpage.com/topics/games']"
    video_link = "a[href='//fandom.yourpage.com/video']"
    wikis_link = "div[class='wds-global-navigation__wikis-menu wds-dropdown']"
    user_avatar = "img[class='wds-avatar wds-is-circle']"
