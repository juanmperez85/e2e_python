from __future__ import unicode_literals
from core.base_page_wikia import BasePageWikia


class LoginPage(BasePageWikia):
    url = "/signin"
    username = "#loginUsername"
    password = "#loginPassword"
    login_button = "#loginSubmit"
    confirm_cookies = "/html/body/div[1]/div/div/div[3]/div[1]"
