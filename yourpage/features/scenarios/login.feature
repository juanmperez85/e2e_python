Feature: Accessing to Wikia

  Scenario: Login
    Given log in with the configured user
    Then the "games_link" element is displayed in "Menu Page"
    And the "movies_link" element is displayed in "Menu Page"
    And the "tv_link" element is displayed in "Menu Page"
    And the "video_link" element is displayed in "Menu Page"
    And the "wikis_link" element is displayed in "Menu Page"
    And the "user_avatar" element is displayed in "Menu Page"