# e2e_python
Using Selenium WebDriver and JBehave, this is a lite e2e framework based in Python

# System
Ubuntu version > 16.04

# Preconditions
sudo apt-get install git (Smoke tests)
sudo apt-get install openjdk-6-jdk
sudo apt-get install python-pip python-dev build-essential

# Installing virtual env
sudo pip install virtualenv 
sudo pip install virtualenvwrapper

# Installing e2e framework dependencies
sudo apt-get install xserver-xephyr
sudo apt-get install xvfb
sudo apt-get install chromium-chromedriver

##### Downloading the project
Into your local machine, create a common code folder. For example "/home/youruser/code/"
Then, create a folder for this project, for example "/e2e"
```
mdkir code
mkdir e2e
```
Access to the project folder. Example "cd /home/youruser/code/e2e"
Then download the repository
```
git clone https://gitlab.com/juanmperez85/e2e_python.git
```

Check that a new folder has been created in your project folder
Example: "/home/youruser/code/e2e/e2e_python"
where "e2e_python" contains the code project

# Creating the virtual environment
Using "workon" or "virtualenv", create the virtual environment
It should be created out of the project folder.
Example where it should be created "/home/youruser/code/e2e"
Example where it should not be created "/home/youruser/code/e2e/e2e_python"
Example where it should not be created "/home/youruser/code/"

```
virtualenv _env_e2e
```

Normally, the virtual environment is activated by default
In you console you are going to see something like
"(_env_e2e) youruser@MLGPC059:~/code/e2e/e2e_python$"

You can activate it manually accessting to your code folder
Example: cd /home/youruser/code/e2e/e2e_python

And activating it
Example: . ../_env_e2e/bin/activate


# Add ChromeDriver path into your local environment
Open your activate file in an editor

```
# Example using nano editor
nano /home/your_user/code/e2e/_env_e2e/bin/activate
```
Then add the information below at the end of the activate file:

```
#Path to chromedriver
export CHROMEDRIVER_PATH="/usr/lib/chromium-browser/chromedriver"
```

Then, deactivate your virtual environment (Example: $ deactivate)
Access to the project folcer (Example: cd /home/yourfolder/code/e2e/wikia)
And activate it again (Example: . ../_env_e2e/bin/activate)

##### Install pip dependencies
Into the "code/e2e/e2e_python" folder execute:

```
pip install -r requirements.txt
```

# Example for executing all features in "yourpage" folder
Into the project folder, with the virtual environment activated:
```
python manage.py test yourpage --settings=integration_tests.settings.e2e
```

